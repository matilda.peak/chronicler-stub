---
Title:      The Chronicler-Stub Docker Configuration File Directory
Author:     Alan Christie
Date:       13 April 2019
Copyright:  Matilda Peak Limited. All rights reserved.
---

# image-config
This directory contains files for the production-grade
container image. They are copied to the image during the Docker build.
Files include...

-   `logging.yml`
