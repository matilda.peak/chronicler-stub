#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Chronicler (Stub).
A simple 'mock' version of the full-blown Chronicler.

This is a very light-weight implementation for testing.
It receives events, expected to be from a transmitter like 'garten'
and posts them onto queues created by each outbound (websocket) connection.
"""

import logging
from logging.config import dictConfig
import asyncio
import os
import queue
import yaml

from quart import Quart, json, request, websocket

from matildapeak.chronicler import EventMessage_pb2, EventCategoryEnum_pb2

# Load logger configuration (from cwd)...
# But only if the logging configuration is present!
_LOGGING_CONFIG_FILE = 'logging.yml'
if os.path.isfile(_LOGGING_CONFIG_FILE):
    _LOGGING_CONFIG = None
    with open(_LOGGING_CONFIG_FILE, 'r', encoding='utf-8') as stream:
        try:
            _LOGGING_CONFIG = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    dictConfig(_LOGGING_CONFIG)

# Now it's configured
# (and it should be for all modules as `disable_existing_loggers`
# is set to False in `logging.yml`)
# we can now create our own logger. We use 'chronicler' as the logger here
# otherwise we'll get __main__ when we're run. We can use __main__ for all
# modules that are imported.
_LOGGER = logging.getLogger('chronicler')

app = Quart(__name__)
# Disable distracting logging...
log = logging.getLogger('werkzeug')
log.disabled = True
app.logger.disabled = True

# Message picked up by calls to '/inbound'
_INBOUND_COUNT = 0
# Messages put in queues
# If message 'a' is put in two queues this counts as 2.
_PUT_COUNT = 0
# Number of messages dropped
# (occurs when there are no queues or if a queue is full)
_DROP_COUNT = 0

_OUTBOUND_PORT = int(os.environ.get('OUTBOUND_PORT', '9090'))
_RUNNER_KEY = os.environ.get('RUNNER_KEY', 'abc123')

# A map of event queues indexed by assigned channel number.
# The queue is created in the '/outbound' handler,
# written to from the '/inbound' handler and read from in the
# '/channel' handler.
_EVENT_QUEUES = {}
_EVENT_QUEUE_SIZE = 10000
# The list of connected sockets
_CONNECTIONS = []

# The next channel to assign.
# In this version channels are not re-used
# so the number simply increments for each outbound channel.
_NEXT_CHANNEL_ID = 1

# Always log the Nth received message...
_MSG_LOG_INTERVAL = 100

# The name of the Runner key in the request/websocket header.
# If the runner does not provide the correct value a websocket
# connection is not granted.
_RUNNER_KEY_NAME = 'X-MP-RunnerKey'


@app.route('/inbound', methods=['POST'])
async def inbound():
    """Handles POST messages to /inbound.
    These message contain data to be held and forwarded to connected runners.
    """
    global _DROP_COUNT
    global _INBOUND_COUNT
    global _PUT_COUNT

    # Log every 'N'th message
    if _INBOUND_COUNT % _MSG_LOG_INTERVAL == 0:
        _LOGGER.info('---- inbound')
        _LOGGER.info('websockets: %s', len(_EVENT_QUEUES))
        _LOGGER.info('%s in, %s out, %s dropped',
                     _INBOUND_COUNT, _PUT_COUNT, _DROP_COUNT)
        _LOGGER.info('Current message: %s', request.args.to_dict())
    elif _LOGGER.isEnabledFor(logging.DEBUG):
        # And log every other (if debugging)
        _LOGGER.debug('---- inbound')
        _LOGGER.debug('websockets: %s', len(_EVENT_QUEUES))
        _LOGGER.debug('%s in, %s out, %s dropped',
                      _INBOUND_COUNT, _PUT_COUNT, _DROP_COUNT)
        _LOGGER.debug('Current message: %s', request.args.to_dict())

    # Just record the reception of a message
    _INBOUND_COUNT += 1

    # Must have some data
    if not request.args.to_dict():
        return '', 400
    # Do nothing (except return success)
    # if there are no outbound queues...
    if not _EVENT_QUEUES:
        _DROP_COUNT += 1
        _LOGGER.debug('No event queues')
        return '', 201

    # Construct an EventMessage
    # based on the content of the inbound arguments.
    # The POST arguments become the event message header.
    #
    # For garten the inbound arguments would typically be
    # a dictionary like this: -
    #
    #   {'axis': '284',
    #    'bikes': '15',
    #    'broken docks': '0',
    #    'docks': '20',
    #    'name': 'Lambeth North Station, Waterloo',
    #    'spaces': '5',
    #    'time': '2019-04-08T21:22:24Z',
    #    'working docks': '20'}
    #
    event_message = EventMessage_pb2.EventMessage()
    event_message.category = EventCategoryEnum_pb2. \
        EVENT_CATEGORY_CUSTOMER_DATA
    event_message.header.update(request.args.to_dict())

    # Post the message to each outbound queue...
    if not _EVENT_QUEUES:
        _DROP_COUNT += 1
    else:
        for channel_id in _EVENT_QUEUES:  # pylint: disable=consider-using-dict-items
            event_queue = _EVENT_QUEUES[channel_id]
            try:
                event_queue.put(event_message, False)
                _PUT_COUNT += 1
            except queue.Full:
                _DROP_COUNT += 1
                _LOGGER.warning('Channel %s (FULL) [at message %s]',
                                channel_id, _INBOUND_COUNT)

    # Done
    return '', 201


@app.route('/outbound', methods=['POST'])
async def outbound():
    """Handles POST messages to /outbound.
    It returns a 201 status response with web-socket information.
    See the Runner's chronicler_runner.py _try_to_extract_ws() method
    for details of the response information.
    """
    global _NEXT_CHANNEL_ID

    # Just record the reception of a message
    _LOGGER.info('---- outbound')

    # There must be a Runner Key in the header.
    # If not then the request is a Bad Request (400).
    # If the key's there but it does not match
    # then the call's Unauthorized (401).
    runner_key = request.headers.get(_RUNNER_KEY_NAME)
    if not runner_key:
        _LOGGER.warning('No runner key')
        return '', 400
    if runner_key != _RUNNER_KEY:
        _LOGGER.warning('Invalid runner key')
        return '', 401

    # Assign an outbound ID
    channel_id = _NEXT_CHANNEL_ID
    _NEXT_CHANNEL_ID += 1
    _LOGGER.info('Assigning new channel (%s)', channel_id)

    # Create a new queue (with fixed size)
    _EVENT_QUEUES[channel_id] = queue.Queue(_EVENT_QUEUE_SIZE)

    # Construct a valid response
    #
    # - It's a 201 response
    # - The body/data contains an id, uri and version
    # - Headers must contain a 'Location' where the date can be
    #   read from, typically 'ws://chronicler.chronicler:9090/channel/1'
    #
    hostname = 'chronicler.chronicler'
    uri = f'ws://{hostname}:{_OUTBOUND_PORT}/channel/{channel_id}'
    data = {'id': channel_id,
            'uri': uri,
            'version': '2019.1'}
    resp = json.jsonify(data)
    resp.status_code = 201
    resp.headers['Location'] = uri

    return resp


@app.websocket('/channel/<int:channel_id>')
async def ws(channel_id):
    """Handles the websocket connection for a channel.
    If the channel is valid it simply reads from the expected queue for the
    channel and sends the message (a protocol buffer) to the socket.

    :param channel_id: The assigned channel ID
    """

    int_channel_id = int(channel_id)
    _LOGGER.info('---- websocket (%s)', int_channel_id)
    _LOGGER.info('Header: %s', websocket.headers.to_dict())

    # There must be a Runner Key in the header.
    # If not then the websocket is a Bad Request (400).
    # If the key's there but it does not match
    # then the call's Unauthorized (401).
    runner_key = websocket.headers.get(_RUNNER_KEY_NAME)
    if not runner_key:
        _LOGGER.warning('No runner key (%s)', int_channel_id)
        return '', 400
    if runner_key != _RUNNER_KEY:
        _LOGGER.warning('Incorrect runner key (%s)', int_channel_id)
        return '', 401

    # It's Forbidden (403) to connect to a connected socket
    if int_channel_id in _CONNECTIONS:
        _LOGGER.warning('Connection already made (%s)', int_channel_id)
        return '', 403

    # If there is no queue for this channel, leave as Not Found (404).
    channel_queue = _EVENT_QUEUES.get(int_channel_id)
    if not channel_queue:
        _LOGGER.warning('No queue (%s)', int_channel_id)
        return '', 404

    # OK if we get here...

    _LOGGER.info('Connected (%s)', int_channel_id)
    _CONNECTIONS.append(int_channel_id)

    while True:

        # We must not block
        event_message = None
        try:
            event_message = channel_queue.get(False)
        except queue.Empty:
            pass

        # Regardless of whether we have a message,
        # we have to 'await' something otherwise other
        # websocket connections cannot be made.
        if event_message:
            await websocket.send(event_message.SerializeToString())
        else:
            await asyncio.sleep(4)
