---
Title:      The Chronicler (Stub) Project
Author:     Alan Christie
Date:       11 May 2022
Copyright:  Matilda Peak. All rights reserved.
---

[![pipeline status](https://gitlab.com/matilda.peak/chronicler-stub/badges/master/pipeline.svg)](https://gitlab.com/matilda.peak/chronicler-stub/commits/master)

# Chronicler (Stub)
A stub (mock) of the Chronicler framework.

A _compact_ but limited implementation of the Chronicler framework,
designed to allow 'mock' testing of Matilda Peak applications in
confined/embedded environments (like Kubernetes on the [Raspberry Pi]
with [PiKs]).

## Delivering events (messages)
Messages (with material passed in via the URI's _parameters_) are `POST`ed
(typically) to `http://chronicler.chronicler:9090/inbound`.

Events are converted to EventMessage protocol buffers and put onto queues for
delivery to websocket-based connections.

>   The queues are created when a request for a websocket is received
    (see below) with a separate queue for each websocket.

Successful `POST`s are greeted with a `201` response.

## Collecting events
Before a client can collect Chronicler events they have to request a websocket
and then connect to it.

### Request a websocket
To obtain a websocket a `POST` is made to
`http://chronicler.chronicler:9090/outbound`. For success the client (a Runner)
must supply a matching `X-Runner-Key` in the `POST` header.

If successful a `201` response is received with a _Header_ `Location` field
that contains the websocket URI as the value
(typically `http://chronciler.chronicler:9090/channel/1`).

>   Additionally, to mirror the real Chronicler, the response body (data)
    contains an `id` (the channel ID), the websocket `uri` and a `version`
    string.

### Connect and receive
Clients (Runners) `connect()` to the _Location_ obtained in their call to
`/outbound` and repeatedly call `recv()` to receive event protocol buffers.

>   Like the call to `/outbound` the connect call must provide a matching
    `X-Runner-Key` for the connect to succeed.

# Container Building
There's a Dockerfile to build the Docker image. Main building is done
via the GitLab CI service (see `.gitlab-ci.yml`).

---

[piks]: https://gitlab.com/a.b.christie/piks
[raspberry pi]: https://www.raspberrypi.org
