import unittest
import pytest

import json
import chronicler


TEST_RESOURCE = '/inbound'


class TestChronicler(unittest.TestCase):

    @pytest.mark.asyncio
    async def test_test_event_without_payload(self):
        app = chronicler.app.test_client()
        response = app.post(TEST_RESOURCE)
        self.assertEqual(201, response.status_code)

    @pytest.mark.asyncio
    async def test_test_event_with_payload(self):
        app = chronicler.app.test_client()
        response = app.post(TEST_RESOURCE,
                            data=json.dumps(dict({'a': 1})),
                            content_type='application/json')
        self.assertEqual(201, response.status_code)
