# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

ARG from_image=python:3.9.12-alpine3.15
FROM ${from_image}

# Labels
LABEL maintainer='Matilda Peak <info@matildapeak.com>'

# Force the binary layer of the stdout and stderr streams
# to be unbuffered
ENV PYTHONUNBUFFERED 1

# Base directory for the application
# Also used for user directory
ENV APP_ROOT /home/chronicler

# Containers should NOT run as root
# (as good practice)
RUN adduser -D -h ${APP_ROOT} -s /bin/sh chronicler
RUN chown -R chronicler.chronicler ${APP_ROOT}

COPY requirements.txt ${APP_ROOT}/
RUN pip install -r ${APP_ROOT}/requirements.txt

COPY chronicler.py ${APP_ROOT}/
COPY wsgi.py ${APP_ROOT}/
COPY image-config/logging.yml ${APP_ROOT}/
COPY LICENCE.txt ${APP_ROOT}/

COPY requirements.txt ${APP_ROOT}/
RUN pip install -r ${APP_ROOT}/requirements.txt

USER chronicler
ENV HOME ${APP_ROOT}
WORKDIR ${APP_ROOT}

CMD hypercorn -w 1 --bind 0.0.0.0:9090 wsgi:app
